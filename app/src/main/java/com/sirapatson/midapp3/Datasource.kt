package com.sirapatson.midapp3

class Datasource {
    fun loadOil() :List<Oil>{
        return listOf<Oil>(
            Oil(R.string.nameOil1,R.string.priceOil1),
            Oil(R.string.nameOil2,R.string.priceOil2),
            Oil(R.string.nameOil3,R.string.priceOil3),
            Oil(R.string.nameOil4,R.string.priceOil4),
            Oil(R.string.nameOil5,R.string.priceOil5),
            Oil(R.string.nameOil6,R.string.priceOil6),
            Oil(R.string.nameOil7,R.string.priceOil7),
            Oil(R.string.nameOil8,R.string.priceOil8),
            Oil(R.string.nameOil9,R.string.priceOil9),
        )
    }
}
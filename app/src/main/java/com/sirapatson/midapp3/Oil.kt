package com.sirapatson.midapp3

import androidx.annotation.StringRes

data class Oil(
    @StringRes val name:Int,
    @StringRes val price:Int)
